package store.noone.ice.meta.vo;

import lombok.Data;

import java.util.Set;

@Data
public class AllocUserRoleVO {
    private Long userId;
    private Set<Long> roleIds;
}
