package store.noone.ice.meta.po;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author 刘晓东
 */
@Data
@Entity
@Table(name = "tb_iot_opt_log")
public class OptLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int optType;
    private String operator;
    private String remark;
    private long createTime;

}
