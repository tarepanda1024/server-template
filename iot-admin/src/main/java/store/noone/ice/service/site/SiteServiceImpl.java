package store.noone.ice.service.site;

import lombok.extern.slf4j.Slf4j;
import store.noone.ice.dao.SiteRepository;
import store.noone.ice.meta.po.Site;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SiteServiceImpl implements SiteService {
    @Autowired
    private SiteRepository siteRepository;

    @Override
    public Site save(Site site) {
        site.setId(100);
        return siteRepository.save(site);
    }

    @Override
    public Site getSiteInfo() {
        return siteRepository.findById(100L).orElse(null);
    }
}
