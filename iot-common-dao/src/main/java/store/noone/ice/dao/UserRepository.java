package store.noone.ice.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import store.noone.ice.meta.po.User;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    User findByUsername(String username);

    Page<User> findByUsernameContaining(String username,  Pageable pageable);

    @Transactional
    @Modifying
    @Query("update User as obj set obj.status = ?2 where obj.id=?1")
    int changeState(long id, int status);

    @Transactional
    @Modifying
    @Query("update User as obj set obj.username = ?1,obj.realName = ?2,obj.phone = ?3,obj.email = ?4,obj.updateTime = ?5,obj.operator = ?6,obj.remark = ?7 where obj.id=?8")
    int updateBaseInfo(String username, String realName, String phone, String email, long updateTime, String operator, String remark, long id);

    @Transactional
    @Modifying
    @Query("update User as obj set obj.username = ?1,obj.realName = ?2,obj.phone = ?3,obj.email = ?4,obj.updateTime = ?5,obj.operator = ?6,obj.remark = ?7 where obj.id=?8")
    int updateInfo(String username, String realName, String phone, String email, long updateTime, String operator, String remark, long id);
}
