package store.noone.ice.meta.po;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "tb_site")
public class Site {
    @Id
    private long id;
    private String siteName;//站点名成
    private String corpName;//公司名称
    private String copyInfo;//备案信息
    private String version;//版本号
}
