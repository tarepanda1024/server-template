package store.noone.mq;

import lombok.Data;

/**
 * @author 刘晓东
 */
@Data
public class RabbitMqConfig {
    private String productName = "setting";
    private String host;
    private int port;
    private String userName;
    private String password;
    private String virtualHost;
    private String queueName = "queue";
    private String exchangeName;
    private boolean autoDelete = false;
    private String exchangeType = "fanout";
    private String routingKey = "*";
}
