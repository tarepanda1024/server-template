package store.noone.ice.service;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import store.noone.ice.dao.PermissionRepository;
import store.noone.ice.meta.bo.PermTreeNode;
import store.noone.ice.meta.bo.UserInfo;
import store.noone.ice.meta.enums.OptTypeEnum;
import store.noone.ice.meta.po.Permission;
import store.noone.ice.security.Security;
import store.noone.ice.service.system.OptService;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
public class PermissionServiceImpl implements PermissionService {

    private final PermissionRepository permissionRepository;
    @Autowired
    private OptService optService;

    @Autowired
    public PermissionServiceImpl(PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    @Override
    public Permission insert(Permission permission) {
        long time = System.currentTimeMillis();
        permission.setCreateTime(time);
        permission.setUpdateTime(time);
        UserInfo userInfo = Security.getUserInfo();
        if (userInfo != null) {
            permission.setOperator(userInfo.getUsername());
        }
        optService.remark(OptTypeEnum.SYSTEM_PERM, "添加系统权限 [" + permission.getName() + "] 成功!");
        return permissionRepository.save(permission);
    }

    @Override
    public Permission update(Permission permission) {
        long time = System.currentTimeMillis();
        permission.setUpdateTime(time);
        UserInfo userInfo = Security.getUserInfo();
        if (userInfo != null) {
            permission.setOperator(userInfo.getUsername());
        }
        optService.remark(OptTypeEnum.SYSTEM_PERM, "更新系统权限 [" + permission.getName() + "] 成功!");
        return permissionRepository.save(permission);
    }

    @Override
    public Permission getById(long id) {
        Optional<Permission> ret = permissionRepository.findById(id);
        return ret.orElse(null);
    }

    @Override
    public Permission getByPermId(int permId) {
        Optional<Permission> ret = permissionRepository.findByPermId(permId);
        return ret.orElse(null);
    }

    private void buildPermTree(PermTreeNode baseTreeNode, Set<Integer> permIds, boolean isSuper) {
        List<Permission> permissions = permissionRepository.findAllByParentId(baseTreeNode.getPermId());
        for (Permission permission : permissions) {
            if (!isSuper && !permIds.contains(permission.getPermId())) {
                continue;
            }
            PermTreeNode permTreeNode = new PermTreeNode();
            permTreeNode.setId(permission.getId());
            permTreeNode.setPermId(permission.getPermId());
            permTreeNode.setName(permission.getName());
            List<PermTreeNode> childrens = baseTreeNode.getChildrens();
            if (childrens == null) {
                childrens = new LinkedList<>();
                baseTreeNode.setChildrens(childrens);
            }
            childrens.add(permTreeNode);
            buildPermTree(permTreeNode, permIds, isSuper);
        }
    }

    @Override
    public PermTreeNode listPermTree() {
        UserInfo userInfo = Security.getUserInfo();
        PermTreeNode baseTreeNode = new PermTreeNode();
        baseTreeNode.setId(-1);
        baseTreeNode.setPermId(0);
        baseTreeNode.setName("root");
        Set<Integer> filterNodes = userInfo.getPermIds();
        buildPermTree(baseTreeNode, filterNodes, "admin".equals(userInfo.getUsername()));
        return baseTreeNode;
    }

    @Override
    public void del(long id) {
        Permission permission = getById(id);
        if (permission != null) {
            optService.remark(OptTypeEnum.SYSTEM_PERM, "删除系统权限 [" + permission.getName() + "] 成功!");
            permissionRepository.deleteById(id);
        }

    }
}
