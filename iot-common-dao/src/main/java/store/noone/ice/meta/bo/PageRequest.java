package store.noone.ice.meta.bo;

import lombok.Data;

@Data
public class PageRequest {
    private String searchStr;
    private int page;
    private int size;

    public long getOffset() {
        return (page - 1) * size;
    }
}
