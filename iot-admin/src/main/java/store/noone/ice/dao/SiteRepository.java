package store.noone.ice.dao;

import store.noone.ice.meta.po.Site;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SiteRepository extends PagingAndSortingRepository<Site, Long> {

}
