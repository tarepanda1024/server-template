package store.noone.ice.meta.vo;

import lombok.Data;

/**
 * Created with IDEA 2019
 *
 * @author Kang
 * @date 2019/7/10
 */
@Data
public class UploadResult {
    private String result;
    private String reason;
}
