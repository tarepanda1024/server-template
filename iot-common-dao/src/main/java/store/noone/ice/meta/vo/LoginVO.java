package store.noone.ice.meta.vo;

import lombok.Data;

@Data
public class LoginVO {
    private String username;
    private String password;
    private String veryCode;
    private String openId;
}
