## 物联网项目部署说明

### 前端项目部署说明
> 前端项目使用的vue+element开发

#### 1.打包前端资源
```
npm run build
```
将生成的dist目录下的所有文件拷贝纸iot-admin中的resources下的static目录下，如果目录不存在，需要建立

#### 2.安装mysql5.7,然后执行如下命令
##### 2.1 创建数据库
```sql
CREATE DATABASE IF NOT EXISTS db_iot DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
```
执行本目录下的init.sql
##### 2.2 插入初始用户
```sql
insert into user(username,realName,salt,password, phone,status,regTime,updateTime,lastLogintime,id)values('admin','admin','bxZU6xacswcd','0c30f04a1bc6a967280d3c1230311acf',
'15557182659',1,1526690234930,1526690234930,1526690234930,100);
```
##### 2.3 插入管理员角色
```sql
insert into role(id,name,status,operator,createTime,updateTime,prefix)values(100,'超级管理员',1,'admin',1526690234930,1526690234930,'[0]_');
insert into user_role(id,userId,roleId)values(100,100,100);
```
##### 2.4 初始化权限
```sql
insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(2000,90,0,'系统管理菜单',0,'system:manage',1526690234930,1526690234930);
insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(2100,9010,90,'用户管理页面',0,'people:view',1526690234930,1526690234930);
insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(2110,901001,9010,'用户新增',1,'people:insert',1526690234930,1526690234930);
insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(2120,901002,9010,'用户编辑',1,'people:edit',1526690234930,1526690234930);
insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(2130,901003,9010,'用户状态',1,'people:state',1526690234930,1526690234930);

insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(2200,9020,90,'角色管理页面',0,'role:view',1526690234930,1526690234930);
insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(2210,902001,9020,'角色新增',1,'role:insert',1526690234930,1526690234930);
insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(2220,902002,9020,'角色编辑',1,'role:edit',1526690234930,1526690234930);
insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(2230,902003,9020,'角色状态',1,'role:state',1526690234930,1526690234930);

insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(2300,9030,90,'资源管理页面',0,'perm:view',1526690234930,1526690234930);
insert into permission(id,permId,parentId,needSuperRole,name,type,permission,createTime,updateTime)values(2310,903001,9030,1,'权限新增',1,'perm:insert',1526690234930,1526690234930);
insert into permission(id,permId,parentId,needSuperRole,name,type,permission,createTime,updateTime)values(2320,903002,9030,1,'权限编辑',1,'perm:edit',1526690234930,1526690234930);
insert into permission(id,permId,parentId,needSuperRole,name,type,permission,createTime,updateTime)values(2330,903003,9030,1,'权限删除',1,'perm:del',1526690234930,1526690234930);
insert into permission(id,permId,parentId,needSuperRole,name,type,permission,createTime,updateTime)values(2340,903000,9030,1,'权限查看',1,'perm:views',1526690234930,1526690234930);


insert into role_permission(id,permId,roleId)values(200,90,100);
insert into role_permission(id,permId,roleId)values(210,9010,100);
insert into role_permission(id,permId,roleId)values(220,901001,100);
insert into role_permission(id,permId,roleId)values(230,901002,100);
insert into role_permission(id,permId,roleId)values(240,901003,100);
insert into role_permission(id,permId,roleId)values(250,9020,100);
insert into role_permission(id,permId,roleId)values(260,902001,100);
insert into role_permission(id,permId,roleId)values(270,902002,100);
insert into role_permission(id,permId,roleId)values(280,902003,100);
insert into role_permission(id,permId,roleId)values(290,9030,100);
insert into role_permission(id,permId,roleId)values(300,903001,100);
insert into role_permission(id,permId,roleId)values(310,903002,100);
insert into role_permission(id,permId,roleId)values(320,903003,100);
insert into role_permission(id,permId,roleId)values(330,903000,100);

-- 20180931新增
insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(3000,9050,90,'站点管理',0,'site:save',1526690234930,1526690234930);
INSERT INTO `db_iot`.`permission`(`id`, `createTime`, `name`, `operator`, `parentId`, `permId`, `permission`, `remark`, `type`, `updateTime`, `url`) VALUES (2140, 1526690234930, '用户删除', NULL, 9010, 901004, 'people:del', NULL, 1, 1526690234930, NULL);
INSERT INTO `db_iot`.`permission`(`id`, `createTime`, `name`, `operator`, `parentId`, `permId`, `permission`, `remark`, `type`, `updateTime`, `url`) VALUES (2240, 1526690234930, '角色删除', NULL, 9020, 902004, 'role:del', NULL, 1, 1526690234930, NULL);
INSERT INTO `db_iot`.`permission`(`id`, `parentId`, `permId`, `needSuperRole`, `type`, `name`, `url`, `permission`, `createTime`, `updateTime`, `operator`, `remark`) VALUES (2821, 3030, 303003, 0, 1, '传感器类型删除', NULL, 'sensortype:del', 1526690234930, 1526690234930, NULL, NULL);
INSERT INTO `db_iot`.`permission`(`id`, `parentId`, `permId`, `needSuperRole`, `type`, `name`, `url`, `permission`, `createTime`, `updateTime`, `operator`, `remark`) VALUES (2860, 3050, 305003, 0, 1, '设备类型删除', NULL, 'devicetype:del', 1526690234930, 1526690234930, NULL, NULL);

```

#### 3.安装rabbitmq
docker exec -it mysql env LANG=C.UTF-8 bash
新增用户iot-admin   V1de01ab    并设置超级管理员权限，删除guest用户

在rabbitmq的管理界面创建如下用户

用户:iot-setting 密码: iot-setting vhost: /iot-setting exchange:iot-setting exchangeType:fanout
用户: iot-req   密码:iot-req vhost: /iot-req exchange: iot-req exchangeType:fanout
用户: iot-rsp   密码:iot-rsp vhost: /iot-rsp exchange: iot-rsp exchangeType:topic

#### 4.在管理平台创建设备，创建设备类型，创建传感器类型