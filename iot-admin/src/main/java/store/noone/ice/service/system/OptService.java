package store.noone.ice.service.system;

import org.springframework.data.domain.Page;
import store.noone.ice.meta.enums.OptTypeEnum;
import store.noone.ice.meta.po.OptLog;

/**
 * @author 刘晓东
 */
public interface OptService {
    void remark(OptTypeEnum optType, String logMsg);

    void remark(OptTypeEnum optType, String username, String logMsg);

    Page<OptLog> listByPage(String requestStr, int page, int size);

}
