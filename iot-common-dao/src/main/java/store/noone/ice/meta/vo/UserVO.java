package store.noone.ice.meta.vo;

import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class UserVO {
    private long id;
    private long corpId;
    private String prefix;
    private String corpName;
    private String username;
    private String realName;
    private String password;
    private String phone;
    private String email;
    private Set<Long> roleIds;
    private List<String> roles;
    private int status;
    private long regTime;
    private String remark;
}
