package store.noone.ice.meta.enums;

/**
 * @author 刘晓东(liuxd2017 @ 163.com)
 */
public enum OptTypeEnum {
    LOGIN(1, "登录"),
    LOGOUT(2, "登出"),
    CHECKVERYCODE(17,"验证码"),

    CORP(3, "组织结构"),

    SYSTEM_USER(14, "系统人员"),
    SYSTEM_ROLE(15, "系统角色"),
    SYSTEM_PERM(16, "系统权限");
    private final int type;
    private final String desc;

    OptTypeEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
