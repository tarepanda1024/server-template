package store.noone.ice.controller;

import lombok.extern.slf4j.Slf4j;
import store.noone.ice.meta.AjaxResult;
import store.noone.ice.meta.bo.PermTreeNode;
import store.noone.ice.meta.bo.UserInfo;
import store.noone.ice.meta.po.Permission;
import store.noone.ice.meta.po.Role;
import store.noone.ice.security.Security;
import store.noone.ice.service.PermissionService;
import store.noone.ice.service.RoleService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/xhr/admin/perm/")
@Slf4j
public class PermController {

    private final PermissionService permissionService;

    @Autowired
    private RoleService roleService;

    @Autowired
    public PermController(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @PostMapping("/insert")
    @RequiresPermissions("perm:insert")
    public AjaxResult insert(@RequestBody Permission permission) {
        log.info("[insert] begin insert perm ={}", permission);
        if (StringUtils.isEmpty(permission.getName()) || StringUtils.isEmpty(permission.getPermission())) {
            return new AjaxResult<>(HttpStatus.BAD_REQUEST.value(), "参数非法", null);
        }
        Permission retObj = permissionService.insert(permission);
        if (retObj == null) {
            log.info("[insert] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[insert] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", retObj);
        }
    }

    @PostMapping("/update")
    @RequiresPermissions("perm:edit")
    public AjaxResult update(@RequestBody Permission permission) {
        log.info("[update] begin update perm ={}", permission);
        if (StringUtils.isEmpty(permission.getName()) || StringUtils.isEmpty(permission.getPermission())) {
            return new AjaxResult<>(HttpStatus.BAD_REQUEST.value(), "参数非法", null);
        }
        Permission retObj = permissionService.update(permission);
        if (retObj == null) {
            log.info("[update] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[update] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", retObj);
        }
    }

    @GetMapping("/getPermTree")
    @RequiresPermissions("perm:views")
    public AjaxResult getPermTree() {
        PermTreeNode permTreeNode = permissionService.listPermTree();
        if (permTreeNode == null) {
            log.info("[getPermTree] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[getPermTree] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", permTreeNode);
        }
    }

    @GetMapping("/getById")
    public AjaxResult getById(@RequestParam long id) {
        log.info("[getById] id={}", id);
        Permission permission = permissionService.getById(id);
        if (permission == null) {
            log.info("[getById] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[getById] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", permission);
        }
    }

    @GetMapping("/del")
    @RequiresPermissions("perm:del")
    public AjaxResult del(@RequestParam long id) {
        log.info("[del] del perm id={}", id);
        permissionService.del(id);
        return AjaxResult.initOkMsg("success");
    }

    @GetMapping("/getPermIds")
    public AjaxResult getPermIds() {
        UserInfo userInfo = Security.getUserInfo();
        if (userInfo != null) {
            Set<Integer> permIds = new HashSet<>();
            for (Role role : userInfo.getRoles()) {
                List<Permission> permissions = roleService.getAllPermsByRoleId(role.getId());
                for (Permission permission : permissions) {
                    permIds.add(permission.getPermId());
                }
                if ("超级管理员".equals(role.getName())) {
                    permIds.add(90);
                    permIds.add(9010);
                    permIds.add(9020);
                    permIds.add(9030);
                    permIds.add(9040);
                }
            }
            return new AjaxResult(HttpStatus.OK.value(), "success", permIds);
        }
        return AjaxResult.initFailMsg("获取用户权限失败");
    }
}
