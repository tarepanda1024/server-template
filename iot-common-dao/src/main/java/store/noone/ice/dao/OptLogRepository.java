package store.noone.ice.dao;

import store.noone.ice.meta.po.OptLog;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author 刘晓东(liuxd2017 @ 163.com)
 */
@Repository
public interface OptLogRepository extends PagingAndSortingRepository<OptLog, Long> {

    Page<OptLog> findByRemarkContainingOrderByCreateTimeDesc(String name,Pageable pageable);
}
