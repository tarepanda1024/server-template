package store.noone.ice.service;

import store.noone.ice.meta.bo.PermTreeNode;
import store.noone.ice.meta.po.Permission;

public interface PermissionService {
    Permission insert(Permission permission);

    Permission update(Permission permission);

    Permission getById(long id);

    Permission getByPermId(int permId);

    PermTreeNode listPermTree();

    void del(long id);
}
