package store.noone.ice.service.utils;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 刘晓东
 */
@Service
@Slf4j
public class ExcelServiceImpl implements ExcelService {

    @Override
    public List<Map<String, String>> importData(MultipartFile file, List<String> metaDatas) {
        Workbook workbook = null;

        try {
            workbook = getReadWorkBookType(file);
            List<Map<String, String>> contents = Lists.newArrayList();
            //获取第一个sheet
            Sheet sheet = workbook.getSheetAt(0);
            //第0行是表名，忽略，从第二行开始读取
            for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
                Row row = sheet.getRow(rowNum);
                Map<String, String> datas = new HashMap<>();
                for (int colNum = 0; colNum < row.getLastCellNum(); colNum++) {
                    Cell cell = row.getCell(colNum);
                    log.info("[colNum] {}", colNum);
                    String ret = getCellStringVal(cell).trim();
                    datas.put(metaDatas.get(colNum), ret);
                }
                contents.add(datas);

            }
            return contents;
        } catch (Exception e) {
            log.error("[importData] e={}", e);
            return null;
        } finally {
            IOUtils.closeQuietly(workbook);
        }
    }

    private Workbook getReadWorkBookType(MultipartFile file) {
        //xls-2003, xlsx-2007
        InputStream is = null;
        try {
            String originName = file.getOriginalFilename();
            is = file.getInputStream();
            if (originName.toLowerCase().endsWith("xlsx")) {
                return new XSSFWorkbook(is);
            } else if (originName.toLowerCase().endsWith("xls")) {
                return new HSSFWorkbook(is);
            } else {
                log.error("[getReadWorkBookType] unsupport file name.{}", file.getOriginalFilename());
            }
        } catch (IOException e) {
            log.error("[getReadWorkBookType] e={}", e);
        } finally {
            IOUtils.closeQuietly(is);
        }
        return null;
    }

    private String getCellStringVal(Cell cell) {
        if (cell == null) {
            return "";
        }
        CellType cellType = cell.getCellTypeEnum();
        switch (cellType) {
            case NUMERIC:
                cell.setCellType(CellType.STRING);
                return cell.getRichStringCellValue().getString();
            case STRING:
                return cell.getStringCellValue();
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            case FORMULA:
                return cell.getCellFormula();
            case BLANK:
                return "";
            case ERROR:
                return String.valueOf(cell.getErrorCellValue());
            default:
                return "";
        }
    }

}
