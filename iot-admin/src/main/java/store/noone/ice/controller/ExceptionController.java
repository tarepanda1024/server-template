package store.noone.ice.controller;

import lombok.extern.slf4j.Slf4j;
import store.noone.ice.exception.IotException;
import store.noone.ice.exception.UnauthorizedException;
import store.noone.ice.meta.AjaxResult;

import java.util.List;

import org.apache.shiro.ShiroException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class ExceptionController {

    // 捕捉shiro的异常
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(ShiroException.class)
    public AjaxResult handle401(ShiroException e) {
        return new AjaxResult(HttpStatus.UNAUTHORIZED.value(), "没有权限访问", null);
    }

    // 捕捉UnauthorizedException
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(UnauthorizedException.class)
    public AjaxResult handle401() {
        return new AjaxResult(HttpStatus.UNAUTHORIZED.value(), "UnauthorizedException", null);
    }

    // 捕捉其他所有异常
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.OK)
    public AjaxResult methodArgumentNotValidException(MethodArgumentNotValidException ex) {
        log.error("[globalException]", ex);
        StringBuffer errorMsg = new StringBuffer();
        List<ObjectError> errors = ex.getBindingResult().getAllErrors();
        errors.forEach(x -> errorMsg.append(x.getDefaultMessage()).append(";"));
        return new AjaxResult(HttpStatus.BAD_REQUEST.value(), errorMsg.toString(), null);
    }

    @ExceptionHandler(IotException.class)
    @ResponseStatus(HttpStatus.OK)
    public AjaxResult iotException(Throwable ex) {
        log.error("[iotException]", ex);
        return new AjaxResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage(), null);
    }

    // 捕捉其他所有异常
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.OK)
    public AjaxResult globalException(Throwable ex) {
        log.error("[globalException]", ex);
        return new AjaxResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage(), null);
    }

}
