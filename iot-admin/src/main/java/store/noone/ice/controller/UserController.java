package store.noone.ice.controller;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import store.noone.ice.meta.AjaxResult;
import store.noone.ice.meta.bo.CaptchaInfo;
import store.noone.ice.meta.bo.UserInfo;
import store.noone.ice.meta.enums.OptTypeEnum;
import store.noone.ice.meta.vo.*;
import store.noone.ice.security.Security;
import store.noone.ice.service.UserService;
import store.noone.ice.service.system.OptService;
import store.noone.ice.service.utils.MailService;
import store.noone.ice.utils.JWTUtil;
import store.noone.ice.utils.RandomUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping("/xhr/admin/user")
public class UserController {
    private final UserService userService;
    @Autowired
    private OptService optService;
    @Autowired
    private DefaultKaptcha captchaProducer;

    @Autowired
    private MailService mailService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/insert")
    @RequiresPermissions("people:insert")
    public AjaxResult insert(@RequestBody UserVO userVO) {
        log.info("[insert] begin insert user ={}", userVO);
        if (StringUtils.isEmpty(userVO.getUsername()) || StringUtils.isEmpty(userVO.getPhone())) {
            return new AjaxResult<>(HttpStatus.BAD_REQUEST.value(), "参数非法", null);
        }
        try {
            userService.insert(userVO);
        } catch (Exception e) {
            if (e.getMessage().contains("ConstraintViolationException")) {
                throw new RuntimeException("用户名已经存在");
            }
            throw new RuntimeException(e);
        }
        log.info("[insert] success.");
        return new AjaxResult<>(HttpStatus.OK.value(), "success", null);
    }

    @PostMapping("/update")
    @RequiresPermissions("people:edit")
    public AjaxResult update(@RequestBody UserVO userVO) {
        log.info("[update] begin insert user ={}", userVO);
        if (StringUtils.isEmpty(userVO.getUsername()) || StringUtils.isEmpty(userVO.getPhone())) {
            return new AjaxResult<>(HttpStatus.BAD_REQUEST.value(), "参数非法", null);
        }
        int ret = userService.update(userVO);
        if (ret < 1) {
            log.info("[update] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[update] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", null);
        }
    }

    @PostMapping("/updatecurrentuser")
    @RequiresPermissions("people:edit")
    public AjaxResult updateCurrentUser(@RequestBody UserVO userVO) {
        log.info("[update current user ] begin insert user ={}", userVO);
        if (StringUtils.isEmpty(userVO.getUsername()) || StringUtils.isEmpty(userVO.getPhone())) {
            return new AjaxResult<>(HttpStatus.BAD_REQUEST.value(), "参数非法", null);
        }
        int ret = userService.updateCurrentUser(userVO);
        if (ret < 1) {
            log.info("[update] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[update] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", null);
        }
    }

    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginVO loginVO, @SessionAttribute String veryCode) {
        log.info("[checkverycode] begin check verycode.");
        if (!userService.checkVeryCode(veryCode, loginVO.getVeryCode())) {
            log.info("[checkVeryode]  verycode wrong.");
            optService.remark(OptTypeEnum.CHECKVERYCODE, loginVO.getUsername(), "尝试登录系统,验证码错误");
            return AjaxResult.initFailMsg("验证码错误");
        }
        log.info("[checkVeryode] verycode right.");
        log.info("[login] begin login = {}", loginVO.getUsername(), loginVO.getPassword());
        if (StringUtils.isEmpty(loginVO.getUsername()) || StringUtils.isEmpty(loginVO.getPassword()) || StringUtils.isEmpty(loginVO.getVeryCode())) {
            return new AjaxResult<>(HttpStatus.BAD_REQUEST.value(), "参数非法", null);
        }
        String ret = userService.login(loginVO);
        if ("-1".equals(ret)) {
            log.info("[login] failed.");
            optService.remark(OptTypeEnum.LOGIN, loginVO.getUsername(), "尝试登录系统");
            return AjaxResult.initFailMsg("用户不存在");
        } else if ("0".equals(ret)) {
            optService.remark(OptTypeEnum.LOGIN, loginVO.getUsername(), "尝试登录系统,密码错误");
            return AjaxResult.initFailMsg("用户名或密码错误");
        } else {
            log.info("[login] success.");
            optService.remark(OptTypeEnum.LOGIN, loginVO.getUsername(), "成功登录系统");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", ret);
        }
    }



    @PostMapping("/changePwd")
    public AjaxResult changePwd(@RequestBody ChangePwdVO changePwdVO) {
        log.info("[changePwd] begin changePwd");
        if (StringUtils.isEmpty(changePwdVO.getOriginPwd()) || StringUtils.isEmpty(changePwdVO.getNewPwd())) {
            return new AjaxResult<>(HttpStatus.BAD_REQUEST.value(), " 参数非法", null);
        }
        UserInfo userInfo = (UserInfo) SecurityUtils.getSubject().getPrincipal();
        int code = userService.changePwd(userInfo.getId(), changePwdVO.getOriginPwd(), changePwdVO.getNewPwd());
        if (code == -1) {
            log.info("[changePwd] failed.");
            return AjaxResult.initFailMsg("用户不存在");
        } else if (code == 0) {
            return AjaxResult.initFailMsg("旧密码错误");
        } else {
            log.info("[changePwd] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", null);
        }
    }

    @PostMapping("/allocRoles")
    public AjaxResult allocRoles(@RequestBody AllocUserRoleVO allocUserRoleVO) {
        log.info("[allocRoles] begin allocRoles ={}", allocUserRoleVO);
        if (StringUtils.isEmpty(allocUserRoleVO.getUserId())) {
            return new AjaxResult<>(HttpStatus.BAD_REQUEST.value(), "参数非法", null);
        }
        if (userService.allocRole(allocUserRoleVO.getUserId(), allocUserRoleVO.getRoleIds())) {
            log.info("[allocRoles] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", null);

        } else {
            log.info("[allocRoles] failed.");
            return AjaxResult.initFailMsg("内部错误");
        }
    }

    @GetMapping("/resetPwd")
    @RequiresPermissions("people:edit")
    public AjaxResult resetPwd(@RequestParam long id) {
        int count = userService.resetPwd(id);
        if (count < 1) {
            log.info("[lock] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[lock] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", count);
        }
    }

    @GetMapping("/refresh-token")
    @RequiresAuthentication
    public AjaxResult refreshToken() {
        UserInfo userInfo = Security.getUserInfo();
        if (userInfo != null) {
            String token = JWTUtil.sign(userInfo.getUsername(), userInfo.getPassword());
            return new AjaxResult<>(HttpStatus.OK.value(), "success", token);
        } else {
            return new AjaxResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), "内部错误", null);
        }

    }

    @PostMapping("/list")
    @RequiresPermissions("people:view")
    public AjaxResult list(@RequestBody QueryVO queryVO) {
        log.info("[list] begin list user");
        if (queryVO.getPage() < 1 || queryVO.getSize() < 0) {
            log.error("[list]");
        }

        Page<UserVO> users = userService.listByPage( queryVO.getQueryStr(),
                queryVO.getPage() - 1, queryVO.getSize());
        if (users == null) {
            log.info("[list] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[list] success. {}", users);
            return new AjaxResult<>(HttpStatus.OK.value(), "success", users);
        }
    }

    @GetMapping("/del")
    @RequiresPermissions("people:del")
    public AjaxResult del(@RequestParam long id) {
        if (id < 1) {
            return new AjaxResult<>(HttpStatus.BAD_REQUEST.value(), "success", null);
        }
        userService.del(id);

        return AjaxResult.initOk("success", null);
    }

    @GetMapping("/lock")
    @RequiresPermissions("people:state")
    public AjaxResult lock(@RequestParam long id) {
        int count = userService.lock(id);
        if (count < 1) {
            log.info("[lock] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[lock] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", count);
        }
    }

    @GetMapping("/unlock")
    @RequiresPermissions("people:state")
    public AjaxResult unlock(@RequestParam long id) {
        int count = userService.unlock(id);
        if (count < 1) {
            log.info("[lock] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[lock] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", count);
        }
    }

    @GetMapping("/resign")
    public AjaxResult resign(@RequestParam long id) {
        log.info("[resign] resign id={}", id);
        userService.resign(id);
        return AjaxResult.initOkMsg("success");
    }

    @GetMapping("/getUserInfo")
    public AjaxResult<UserVO> getUserInfo() {
        UserInfo userInfo = Security.getUserInfo();
        UserVO ret = userService.getUserInfo(userInfo.getId());
        ret.setPassword("xxxxxxxxx");
        return new AjaxResult(HttpStatus.OK.value(), "success", ret);
    }

    @RequestMapping("/defaultKaptcha")
    public AjaxResult defaultKaptcha(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        log.info("[getverifycode] getVerifyCode success");
        CaptchaInfo captchaInfo = userService.getKaptchaImage(httpServletRequest, httpServletResponse, captchaProducer);
        return new AjaxResult(HttpStatus.OK.value(), "success", captchaInfo);
    }


    @GetMapping("/sendResetCode")
    public AjaxResult sendResetEmail(@RequestParam String username, @RequestParam String vCode, @SessionAttribute String veryCode) {
        if (!userService.checkVeryCode(veryCode, vCode)) {
            log.info("[verifycode] VerifyCode error!");
            return AjaxResult.initFailMsg("验证码错误");
        }
        UserInfo userInfo = userService.findByUsername(username);
        if (userInfo != null) {
            if (!StringUtils.isEmpty(userInfo.getEmail())) {
                String sentCode = RandomUtils.getRandomString(4);
                log.info("[set code]eamil code={}", sentCode);
                stringRedisTemplate.opsForValue().set(username, sentCode, 600, TimeUnit.SECONDS);//redis设置缓存有效时间10min
                mailService.sendTemplateMail(userInfo.getEmail(), "邮件", sentCode);//校园网不稳定，正式上线取消注释
                return new AjaxResult<>(HttpStatus.OK.value(), "success", null);
            } else if (!StringUtils.isEmpty(userInfo.getPhone())) {
                //:TODO 短信接入
                //mailService.sendTemplateMail("295279385@qq.com", "test simple mail", "hello this is simple mail");
                return new AjaxResult<>(HttpStatus.OK.value(), "success", null);
            }
        }
        return AjaxResult.initFailMsg("请核实你的账户,找回失败");
    }

    @GetMapping("/checkResetCode")
    public AjaxResult checkResetCode(@RequestParam String username, @RequestParam String code) {
        if (code.equals(stringRedisTemplate.opsForValue().get(username))) {
            return new AjaxResult<>(HttpStatus.OK.value(), "success", null);
        }
        log.info("[ResetCode] ResetCode error!");
        return AjaxResult.initFailMsg("邮件验证码错误");
    }

    @GetMapping("/setPwdByFind")
    public AjaxResult setPwdByFind(@RequestParam String username, @RequestParam String code, @RequestParam String password, @RequestParam String password1) {
        if (StringUtils.isEmpty(password) || StringUtils.isEmpty(password1)) {
            return new AjaxResult<>(HttpStatus.BAD_REQUEST.value(), " 参数非法", null);
        }
        if (!password.equals(password1)) {
            log.info("[setPwdByFind] setPwdByFind error!");
            return AjaxResult.initFailMsg("密码不一致");
        }
        if (!code.equals(stringRedisTemplate.opsForValue().get(username))) {
            return AjaxResult.initFailMsg("验证码超时");
        }
        UserInfo userInfo = userService.findByUsername(username);
        int res = userService.findPwd(userInfo.getId(), password);
        if (res == -1) {
            log.info("[changePwd] failed.");
            return AjaxResult.initFailMsg("用户不存在");
        } else if (res == 1) {
            log.info("[changePwd] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", null);
        }
        return AjaxResult.initFailMsg("找回失败，请稍后再试！");
    }
}
