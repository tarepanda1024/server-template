#!/bin/sh
echo ======start $(date +"%Y-%m-%d %H:%M:%S") begin======
CONTAINER_NAME=$(docker ps -a | awk '{ print $1}' | tail -n +2)
docker start $CONTAINER_NAME
if [ "$?"="0" ];then
  echo $CONTAINER_NAME" start ok"
else
  echo $CONTAINER_NAME" start failed"
fi
########################
cd /home/webedit/iot/iot-udp
bash ./iot.sh restart
########################
cd /home/webedit/iot/iot-admin
bash ./iot.sh restart
########################
cd /home/webedit/iot/iot-msg-center
bash ./iot.sh restart
echo ======$(date +"%Y-%m-%d %H:%M:%S") _over======
exit
