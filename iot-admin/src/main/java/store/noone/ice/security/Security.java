package store.noone.ice.security;

import store.noone.ice.exception.IotException;
import store.noone.ice.meta.bo.UserInfo;

import org.apache.shiro.SecurityUtils;

/**
 * @author 刘晓东(liuxd2017 @ 163.com)
 */
public class Security {
    public static UserInfo getUserInfo() {
        Object o = SecurityUtils.getSubject().getPrincipal();
        if (o != null) {
            return (UserInfo) o;
        }
        return null;

    }

    public static void permCheck(String prefix) {
        String realPrefix = getUserInfo().getPrefix();
        if (!prefix.startsWith(realPrefix)) {
            throw new IotException("权限非法");
        }
    }
}
