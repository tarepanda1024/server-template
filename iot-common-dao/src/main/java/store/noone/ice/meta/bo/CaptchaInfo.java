package store.noone.ice.meta.bo;

/**
 * Created with IDEA 2018
 * author:Kang
 * Date:2018/7/22
 * Time:9:19
 */
import lombok.Data;

@Data
public class CaptchaInfo {
    private String img;
    private String code;
}
