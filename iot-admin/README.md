#### 1. 创建数据库
```sql
CREATE DATABASE IF NOT EXISTS db_iot DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
```
#### 2. 插入初始用户
```sql
insert into user(username,realName,salt,password, phone,status,regTime,updateTime,lastLoginTime,id)values('admin','admin','bxZU6xacswcd','0c30f04a1bc6a967280d3c1230311acf',
                                                                                                               '15557182659',1,1526690234930,1526690234930,1526690234930,2);
```
#### 3. 插入管理员角色
```sql
insert into role(id,name,status,operator,createTime,updateTime)values(1,'超级管理员',1,'admin',1526690234930,1526690234930);
insert into user_role(id,userId,roleId)values(1,2,1);
```
#### 4.初始化权限
```sql
insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(1011,90,0,'系统管理',0,'system',1526690234930,1526690234930);
insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(1012,9010,90,'用户管理',0,'people:view',1526690234930,1526690234930);
insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(1013,9020,90,'角色管理',0,'role:view',1526690234930,1526690234930);
insert into permission(id,permId,parentId,name,type,permission,createTime,updateTime)values(1014,9030,90,'权限管理',0,'per,m:view',1526690234930,1526690234930);
```


### 5.　新开发特性需要从dev分支上拉取新分支
