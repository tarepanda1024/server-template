package store.noone.mq;

/**
 * @author 刘晓东
 */

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import store.noone.config.ConfigManager;

public class RabbitMQFactory {
    private static Logger logger = LoggerFactory.getLogger(RabbitMQFactory.class);
    private static ConfigManager configManager = ConfigManager.getInstance();

    public RabbitMQFactory() {
    }

    public static void buildConsumer(RabbitMqConfig rabbitMqConfig, MqConsumer consumer) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(rabbitMqConfig.getHost());
        factory.setPort(rabbitMqConfig.getPort());
        factory.setVirtualHost(rabbitMqConfig.getVirtualHost());
        factory.setUsername(rabbitMqConfig.getUserName());
        factory.setPassword(rabbitMqConfig.getPassword());
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        if (channel == null) {
            throw new RuntimeException("None of RabbitMQ channels are available");
        } else {
            Map params = new HashMap();
            params.put("x-message-ttl", 300000);
            params.put("x-max-length", 50000);
            String queueName = rabbitMqConfig.getQueueName();
            if (rabbitMqConfig.isAutoDelete()) {
                queueName = queueName + "_" + System.currentTimeMillis();
            }
            channel.basicQos(1);
            channel.queueDeclare(queueName, true, rabbitMqConfig.isAutoDelete(), rabbitMqConfig.isAutoDelete(),
                    params);
            channel.queueBind(queueName, rabbitMqConfig.getExchangeName(), rabbitMqConfig.getRoutingKey());
            channel.basicConsume(queueName, true, new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
                                           byte[] body) throws IOException {
                    consumer.handleDelivery(consumerTag, envelope, properties, body);
//                    channel.basicAck(envelope.getDeliveryTag(), false);
                }
            });

        }
    }

    public static RabbitMQProductor buildProductor(RabbitMqConfig rabbitMqConfig) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setVirtualHost(rabbitMqConfig.getVirtualHost());
        factory.setHost(rabbitMqConfig.getHost());
        factory.setPort(rabbitMqConfig.getPort());
        factory.setUsername(rabbitMqConfig.getUserName());
        factory.setPassword(rabbitMqConfig.getPassword());
        Connection conn = factory.newConnection();
        return new RabbitMQProductor(conn, rabbitMqConfig.getExchangeName(), rabbitMqConfig.getExchangeType());
    }

    public static RabbitMQProductor buildProductor(String name) {
        try {
            RabbitMqConfig rabbitMqConfig = new RabbitMqConfig();
            rabbitMqConfig.setHost(configManager.getValue(name + ".rabbit.host"));
            rabbitMqConfig.setPort(configManager.getInt(name + ".rabbit.port", 0));
            rabbitMqConfig.setVirtualHost(configManager.getValue(name + ".rabbit.vhost"));
            rabbitMqConfig.setExchangeName(configManager.getValue(name + ".rabbit.exchange"));
            rabbitMqConfig.setUserName(configManager.getValue(name + ".rabbit.username"));
            rabbitMqConfig.setPassword(configManager.getValue(name + ".rabbit.password"));
            return buildProductor(rabbitMqConfig);
        } catch (Exception e) {
            logger.error("[buildProductor] build " + name + " failed", e);
            throw new RuntimeException(e);
        }

    }

    public static Connection buildConnection(RabbitMqConfig rabbitMqConfig) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setVirtualHost(rabbitMqConfig.getVirtualHost());
        factory.setHost(rabbitMqConfig.getHost());
        factory.setPort(rabbitMqConfig.getPort());
        factory.setUsername(rabbitMqConfig.getUserName());
        factory.setPassword(rabbitMqConfig.getPassword());
        return factory.newConnection();
    }
}
