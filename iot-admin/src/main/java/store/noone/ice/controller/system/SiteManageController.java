package store.noone.ice.controller.system;

import lombok.extern.slf4j.Slf4j;
import store.noone.ice.Const;
import store.noone.ice.meta.AjaxResult;
import store.noone.ice.meta.bo.UserInfo;
import store.noone.ice.meta.po.Site;
import store.noone.ice.security.Security;
import store.noone.ice.service.site.SiteService;
import store.noone.ice.utils.HashUtils;

import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequestMapping("/xhr/admin/site")
public class SiteManageController {

    private static final String SALT = "1654541gsgsheryjfjf";

    @Autowired
    private SiteService siteService;

    @PostConstruct
    public void init() {
        File dir = new File(Const.PIC_DIR);
        if (!dir.exists()) {
            dir.mkdirs();
        }
    }

    @GetMapping("/getInfo")
    public AjaxResult getInfo() {
        return AjaxResult.initOk("success", siteService.getSiteInfo());
    }

    @PostMapping("/saveInfo")
    public AjaxResult saveInfo(@RequestBody Site site) {
        UserInfo userInfo = Security.getUserInfo();
        if (userInfo == null) {
            return null;
        }
        File srcFile = new File(Const.PIC_DIR + getMd5Path(userInfo.getCorpId()) + "/tmp_logo.png");
        if (srcFile.exists()) {
            File dstFile = new File(Const.PIC_DIR + getMd5Path(userInfo.getCorpId()) + "/logo.png");
            if (dstFile.exists()) {
                dstFile.delete();
            }
            srcFile.renameTo(dstFile);
        }

        return AjaxResult.initOk("success", siteService.save(site));
    }

    @GetMapping("/getLogoPath")
    public AjaxResult getLogoPath() {
        UserInfo userInfo = Security.getUserInfo();
        if (userInfo == null) {
            return null;
        }
        String md5 = getMd5Path(userInfo.getCorpId());
        String path = Const.PIC_DIR + getMd5Path(userInfo.getCorpId()) + "/logo.png";
        File file = new File(path);
        if (!file.exists()) {
            md5 = getMd5Path(0);
            return AjaxResult.initOk("success", "/xhr/corp/" + md5 + "/logo.png");
        }
        return AjaxResult.initOk("success", "/xhr/corp/" + md5 + "/logo.png");
    }

    @PostMapping("/uploadPic")
    public AjaxResult uploadImg(@RequestParam("file") MultipartFile file) throws IOException {
        log.info("[uploadImg] begin upload logo");
        UserInfo userInfo = Security.getUserInfo();
        if (userInfo == null) {
            return null;
        }
        File dstFile = new File(Const.PIC_DIR + getMd5Path(userInfo.getCorpId()) + "/tmp_logo.png");
        log.info("[uploadPic] path={}", dstFile.getAbsoluteFile());
        FileUtils.copyInputStreamToFile(file.getInputStream(), dstFile);
        return AjaxResult.initOk("success", null);
    }

    private String getMd5Path(long corpId) {
        return HashUtils.getMd5(corpId + "", SALT, 2);
    }
}
