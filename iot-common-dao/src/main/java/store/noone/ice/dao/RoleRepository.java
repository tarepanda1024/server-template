package store.noone.ice.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import store.noone.ice.meta.po.Role;

import java.util.List;

@Repository
public interface RoleRepository extends PagingAndSortingRepository<Role, Long> {

    Page<Role> findByNameContaining(String name, Pageable pageable);

    @Transactional
    @Modifying
    @Query("update Role as r set r.status = ?2 where r.id=?1")
    int changeState(long id, int status);

    List<Role> findAll();
}
