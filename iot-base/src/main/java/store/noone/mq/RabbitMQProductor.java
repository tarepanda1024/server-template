package store.noone.mq;

/**
 * @author 刘晓东
 */

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

public class RabbitMQProductor {
    private Connection connection;
    private String exchangeName;
    private String exchangeType;

    public RabbitMQProductor(Connection connection, String exchangeName, String exchangeType) {
        this.connection = connection;
        this.exchangeName = exchangeName;
        this.exchangeType = exchangeType;
    }

    public void send(byte[] message) throws Exception {
        send("", message);
    }

    public void send(String routingKey, byte[] message) throws Exception {
        Channel channel = this.connection.createChannel();
        channel.exchangeDeclare(this.exchangeName, exchangeType, true);
        channel.basicPublish(this.exchangeName, routingKey, null, message);
        channel.close();
    }

}

