package store.noone.ice.meta.bo;

import lombok.Data;

import java.util.List;

@Data
public class PermTreeNode {
    private long id;
    private int permId;
    private String name;
    private List<PermTreeNode> childrens;
}
