package store.noone.ice.utils;

import java.security.MessageDigest;

import org.apache.shiro.crypto.hash.SimpleHash;

public class HashUtils {
    public static String getMd5(String source, String salt, int count) {
        String hashAlgorithmName = "MD5";//加密方式
        SimpleHash simpleHash = new SimpleHash(hashAlgorithmName, source,
                salt, count);
        return simpleHash.toHex();
    }

    private static final char[] DIGITS_LOWER = {'0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /**
     * 将字节数组转换为16进制字符串的形式.
     */
    public static final String bytesToHexStr(byte[] bcd) {
        StringBuffer s = new StringBuffer(bcd.length * 2);

        for (int i = 0; i < bcd.length; i++) {
            s.append(DIGITS_LOWER[(bcd[i] >>> 4) & 0x0f]);
            s.append(DIGITS_LOWER[bcd[i] & 0x0f]);
        }

        return s.toString();
    }
    /**
     * 对字符串进行md5加密
     *
     * @param orign
     * @return
     */
    public static String md5(String orign) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(orign.getBytes("UTF-8"));
            byte[] data = md.digest();

            return bytesToHexStr(data);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
