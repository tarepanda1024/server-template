package store.noone.ice.service.utils;


import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IDEA 2018
 *
 * @author Kang
 */
@Service
@Slf4j
public class MailServiceImpl implements MailService{
    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;
    @Value("${mail.fromMail.addr}")
    private  String from;

    @Override
    public void sendSimpleMail(String to, String subject, String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(content);
        try {
            javaMailSender.send(message);
            log.info("[sendSimpleMail]send email succeed,content={}",message);
        } catch (Exception e) {
            log.error("[send email failed]error", e);
        }
    }

    @Override
    public void sendTemplateMail(String to, String subject, String content) {
        MimeMessage message = javaMailSender.createMimeMessage();
        try{
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject(subject);
            Map<String, Object> model = new HashMap<>();
            model.put("content", content);
            Template template = freeMarkerConfigurer.getConfiguration().getTemplate("template.flt");
            String text = FreeMarkerTemplateUtils.processTemplateIntoString(
                    template, model);
            helper.setText(text, true);
            javaMailSender.send(message);
            log.info("[sendSimpleMail]send email succeed,content={}",message);
        }catch (Exception e){
            log.error("[send email failed]error", e);
        }
    }
}
