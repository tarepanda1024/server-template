package store.noone.ice.service;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import store.noone.ice.dao.RolePermissionRepository;
import store.noone.ice.dao.RoleRepository;
import store.noone.ice.meta.bo.UserInfo;
import store.noone.ice.meta.enums.OptTypeEnum;
import store.noone.ice.meta.po.Permission;
import store.noone.ice.meta.po.Role;
import store.noone.ice.meta.po.RolePermission;
import store.noone.ice.meta.vo.RoleVO;
import store.noone.ice.security.Security;
import store.noone.ice.service.system.OptService;

import java.util.*;

@Service
@Slf4j
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    private final RolePermissionRepository rolePermissionRepository;

    private final PermissionService permissionService;
    @Autowired
    private OptService optService;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository, RolePermissionRepository rolePermissionRepository, PermissionService permissionService) {
        this.roleRepository = roleRepository;
        this.rolePermissionRepository = rolePermissionRepository;
        this.permissionService = permissionService;
    }

    @Override
    public int insert(RoleVO roleVO) {
        long time = System.currentTimeMillis();
        log.info("[insert] {}", SecurityUtils.getSubject().getPrincipal());
        Role role = new Role();
        BeanUtils.copyProperties(roleVO, role);
        role.setStatus(1);
        role.setCreateTime(time);
        role.setUpdateTime(time);
        UserInfo userInfo = Security.getUserInfo();
        role.setOperator(userInfo.getUsername());
        Role dstObj = roleRepository.save(role);
        if (dstObj.getId() > 0) {
            optService.remark(OptTypeEnum.SYSTEM_ROLE, "添加系统角色 [" + role.getName() + "] 成功!");
            allocPerm(dstObj.getId(), roleVO.getPermIds());
            return 1;
        }
        optService.remark(OptTypeEnum.SYSTEM_ROLE, "添加系统角色 [" + role.getName() + "] 失败!");
        return 0;
    }

    @Override
    public int update(RoleVO roleVO) {
        Role role = new Role();
        BeanUtils.copyProperties(roleVO, role);
        long time = System.currentTimeMillis();
        role.setUpdateTime(time);
        UserInfo userInfo = Security.getUserInfo();
        if (userInfo != null) {
            role.setOperator(userInfo.getUsername());
        }
        Role dstObj = roleRepository.save(role);
        if (dstObj.getId() > 0) {
            optService.remark(OptTypeEnum.SYSTEM_ROLE, "更新系统角色 [" + role.getName() + "] 成功!");
            allocPerm(dstObj.getId(), roleVO.getPermIds());
            return 1;
        }
        optService.remark(OptTypeEnum.SYSTEM_ROLE, "更新系统角色 [" + role.getName() + "] 失败!");
        return 0;
    }

    @Override
    public Page<RoleVO> listByPage( String requestStr, int page, int size) {
        Page<Role> rolePage = roleRepository.findByNameContaining(requestStr, PageRequest.of(page, size));
        if (rolePage != null) {
            List<Role> pageContent = rolePage.getContent();
            List<RoleVO> dstZoneVOS = new ArrayList<>(pageContent.size());
            for (Role role : pageContent) {
                RoleVO roleVO = new RoleVO();
                BeanUtils.copyProperties(role, roleVO);
                List<Permission> permissions = getAllPermsByRoleId(role.getId());
                if (permissions != null) {
                    List<Integer> permIds = new ArrayList<>();
                    permissions.forEach((permission -> {
                        permIds.add(permission.getPermId());
                    }));
                    roleVO.setPermIds(permIds);
                }
                dstZoneVOS.add(roleVO);
            }
            return new PageImpl<>(dstZoneVOS, rolePage.getPageable(), rolePage.getTotalElements());
        }
        return null;
    }

    @Override
    public int lock(long id) {
        return roleRepository.changeState(id, 0);
    }

    @Override
    public int unlock(long id) {
        return roleRepository.changeState(id, 1);
    }

    @Override
    public void del(long id) {
        Role role = getById(id);
        if (role != null && !"超级管理员".equals(role.getName())) {
            optService.remark(OptTypeEnum.SYSTEM_ROLE, "执行删除角色 [" + role.getName() + "] 操作!");
            roleRepository.deleteById(id);
            rolePermissionRepository.deleteAllByRoleId(id);
        }

    }

    @Override
    public boolean allocPerm(long roleId, List<Integer> permIds) {
        rolePermissionRepository.deleteAllByRoleId(roleId);
        Role role = getById(roleId);
        if (role != null) {
            for (Integer permId : permIds) {
                RolePermission rolePermission = new RolePermission();
                rolePermission.setRoleId(roleId);
                rolePermission.setPermId(permId);
                optService.remark(OptTypeEnum.SYSTEM_ROLE, "给 [" + role.getName() + "] 分配权限成功!");
                rolePermissionRepository.save(rolePermission);
            }
            return true;
        }

        return false;
    }

    @Override
    public List<Permission> getAllPermsByRoleId(long roleId) {
        List<RolePermission> rolePermissions = rolePermissionRepository.findAllByRoleId(roleId);
        if (rolePermissions != null) {
            List<Permission> permissions = new ArrayList<>(rolePermissions.size());
            for (RolePermission rolePermission : rolePermissions) {
                Permission permission = permissionService.getByPermId(rolePermission.getPermId());
                if (permission != null) {
                    permissions.add(permission);
                }
            }
            return permissions;

        }
        return null;
    }

    @Override
    public Set<Integer> getAllPermIdByRoleIds(Set<Long> roleIds) {
        Set<Integer> permissions = new HashSet<>();
        roleIds.forEach(roleId -> {
            List<RolePermission> rolePermissions = rolePermissionRepository.findAllByRoleId(roleId);
            rolePermissions.forEach(rolePermission -> {
                permissions.add(rolePermission.getPermId());
            });
        });

        return permissions;
    }

    @Override
    public JSONObject getAllRoles(String prefix) {
        JSONObject roles = new JSONObject();
        roles.put("current", roleRepository.findAll());
        return roles;
    }

    @Override
    public Role getById(long id) {
        Optional<Role> ret = roleRepository.findById(id);
        return ret.orElse(null);
    }
}
