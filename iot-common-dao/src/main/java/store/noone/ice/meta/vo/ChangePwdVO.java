package store.noone.ice.meta.vo;

import lombok.Data;

/**
 * @author 刘晓东(liuxd2017 @ 163.com)
 */
@Data
public class ChangePwdVO {
    private String originPwd;
    private String newPwd;
}
