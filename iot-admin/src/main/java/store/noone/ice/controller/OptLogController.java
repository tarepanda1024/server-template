package store.noone.ice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import store.noone.ice.meta.AjaxResult;
import store.noone.ice.meta.po.OptLog;
import store.noone.ice.service.system.OptService;

/**
 * @author 刘晓东(liuxd2017 @ 163.com)
 */
@RestController
@RequestMapping("/xhr/admin/optlog/")
@Slf4j
public class OptLogController {
    @Autowired
    private OptService optService;

    @GetMapping("/list")
    public AjaxResult list(@RequestParam(required = false, defaultValue = "") String queryStr, @RequestParam int page, @RequestParam int size) {
        log.info("[list] begin list opts");
        if (page < 1 || size < 0) {
            log.error("[list]");
        }
        Page<OptLog> optLogs = optService.listByPage(queryStr, page - 1, size);
        if (optLogs == null) {
            log.info("[list] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[list] success. {}", optLogs);
            return new AjaxResult<>(HttpStatus.OK.value(), "success", optLogs);
        }
    }
}
