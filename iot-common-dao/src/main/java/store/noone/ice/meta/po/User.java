package store.noone.ice.meta.po;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String realName;
    private String password;
    private String salt;
    private String phone;
    private String email;
    private int status;
    private long regTime = 0;
    private long updateTime = 0;
    private long lastLoginTime = 0;
    private String operator;
    private String remark;
}
