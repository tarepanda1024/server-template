package store.noone.ice.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import store.noone.ice.meta.po.UserRole;

import java.util.List;

@Repository
public interface UserRoleRepository extends CrudRepository<UserRole,Long> {
    @Query("select uesrRole from UserRole uesrRole where uesrRole.userId = ?1")
    List<UserRole> findAllByUserId(long userId);

    @Transactional
    @Modifying
    int deleteAllByUserId(long userId);
}
