package store.noone.ice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class IceApplication {

    public static void main(String[] args) {
        SpringApplication.run(IceApplication.class, args);
    }
}
