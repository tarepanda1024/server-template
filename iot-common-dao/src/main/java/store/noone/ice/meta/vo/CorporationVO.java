package store.noone.ice.meta.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author 刘晓东
 */
@Data
public class CorporationVO {

    private long id;
    private long pid;//父节点id
    @NotNull(message = "组织名称不能为空")
    private String name;
    private String corpNum;
    private String code;
    private String prefix;
    private String manager;
    private String phone;
    private String desc;
    private long createTime;
    private long updateTime;
    private String operator;
    private String remark;
    private List<CorporationVO> childrens;
    private boolean showEdit = true;
    private boolean showInfo = true;
    private boolean showDel = true;
}
