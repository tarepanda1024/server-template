package store.noone.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;
import store.noone.Constant;

/**
 * 该加载器启动时加载一次，不支持自动刷新，Flink使用
 *
 * @author
 */

@Slf4j
public class ConfigManager implements Serializable {
    private static final String CONFIG_PATH = "config.properties";
    private static ConfigManager instance = new ConfigManager();
    private Properties prop;

    private ConfigManager() {
        Properties p = new Properties();
        try {
            InputStream configResource = new ClassPathResource(Constant.env + "/" + CONFIG_PATH).getInputStream();
            p.load(configResource);
        } catch (IOException e) {
            log.error("load prop failed", e);
        }
        prop = p;
    }

    public static ConfigManager getInstance() {
        return instance;
    }


    public String getProperty(String key) {
        return getValue(key) == null ? "" : getValue(key);
    }

    public String getValue(String key) {
        return prop.getProperty(key);
    }

    public String getValue(String key, String defau) {
        String value = getValue(key);
        if (StringUtils.isEmpty(value)) {
            return defau;
        }
        return value;
    }

    public int getInt(String key, int defau) {
        String value = getValue(key);
        if (StringUtils.isEmpty(value)) {
            return defau;
        }
        return Integer.valueOf(value).intValue();
    }

    public long getLong(String key, long defau) {
        String value = getValue(key);
        if (StringUtils.isEmpty(value)) {
            return defau;
        }
        return Long.valueOf(value).longValue();
    }

    public boolean getBoolean(String key, boolean defau) {
        String value = getValue(key);
        if (StringUtils.isEmpty(value)) {
            return defau;
        }
        return Boolean.valueOf(value).booleanValue();
    }

}
