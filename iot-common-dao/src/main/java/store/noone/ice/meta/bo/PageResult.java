package store.noone.ice.meta.bo;

import lombok.Data;

import java.util.List;

@Data
public class PageResult<T> {
    private int page;
    private int size;
    private int totalPage;
    private long totalSize;
    private List<T> datas;

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
        this.totalPage = (int) (totalSize / size);
        if (this.totalPage == 0) {
            this.totalPage = 1;
        }
    }
}
