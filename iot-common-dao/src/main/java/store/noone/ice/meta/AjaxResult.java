package store.noone.ice.meta;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
public class AjaxResult<T> {
    private int code;
    private String msg;
    private T data;

    public static AjaxResult initOkMsg(String msg) {
        return new AjaxResult<>(HttpStatus.OK.value(), msg, null);
    }

    public static AjaxResult initFailMsg(String msg) {
        return new AjaxResult<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg, null);
    }

    public static AjaxResult initOk(String msg, Object data) {
        return new AjaxResult<>(HttpStatus.OK.value(), msg, data);
    }

    public static AjaxResult initFail(String msg, Object data) {
        return new AjaxResult<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg, data);
    }

    public static AjaxResult initBadReqest() {
        return new AjaxResult<>(HttpStatus.BAD_REQUEST.value(), "参数非法", null);
    }
}
