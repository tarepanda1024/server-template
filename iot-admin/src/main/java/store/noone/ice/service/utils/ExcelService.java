package store.noone.ice.service.utils;


import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author 刘晓东
 */
public interface ExcelService {

    List<Map<String, String>> importData(MultipartFile file, List<String> meta);

}
