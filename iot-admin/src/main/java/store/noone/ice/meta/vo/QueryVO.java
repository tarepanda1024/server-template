package store.noone.ice.meta.vo;

import lombok.Data;

@Data
public class QueryVO {
    String queryStr;
    int page;
    int size;
}
