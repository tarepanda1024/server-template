package store.noone.mq;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Envelope;

/**
 * @author 刘晓东
 */
public interface MqConsumer {
    void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
                        byte[] body);
}
