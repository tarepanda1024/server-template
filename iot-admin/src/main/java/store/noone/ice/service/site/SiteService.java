package store.noone.ice.service.site;

import store.noone.ice.meta.po.Site;

public interface SiteService {
    Site save(Site site);

    Site getSiteInfo();
}
