package store.noone.ice.service;

import com.google.code.kaptcha.impl.DefaultKaptcha;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import store.noone.ice.dao.*;
import store.noone.ice.meta.bo.CaptchaInfo;
import store.noone.ice.meta.bo.UserInfo;
import store.noone.ice.meta.po.*;
import store.noone.ice.meta.vo.LoginVO;
import store.noone.ice.meta.vo.UserVO;
import store.noone.ice.security.Security;
import store.noone.ice.utils.HashUtils;
import store.noone.ice.utils.JWTUtil;
import store.noone.ice.utils.RandomUtils;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.*;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final RoleRepository roleRepository;
    @Autowired
    private RoleService roleService;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserRoleRepository userRoleRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
        this.roleRepository = roleRepository;

    }

    @Override
    public UserInfo findByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user != null) {
            UserInfo userInfo = new UserInfo();
            BeanUtils.copyProperties(user, userInfo);
            userInfo.setRoles(getAllRoleByUserId(userInfo.getId()));
            userInfo.setRoleNames(getAllRoleNames(userInfo.getRoles()));
            userInfo.setRoleIds(getAllRoleIds(userInfo.getRoles()));
            userInfo.setPermIds(roleService.getAllPermIdByRoleIds(userInfo.getRoleIds()));
            return userInfo;
        }
        log.info("[findByUsername] username ={} not exist.", username);
        return null;
    }

    private Set<String> getAllRoleNames(List<Role> roles) {
        Set<String> roleNames = new HashSet<>();
        roles.forEach(role -> {
            roleNames.add(role.getName());
        });
        return roleNames;
    }

    private Set<Long> getAllRoleIds(List<Role> roles) {
        Set<Long> roleIds = new HashSet<>();
        roles.forEach(role -> {
            roleIds.add(role.getId());
        });
        return roleIds;
    }

    @Override
    public User insert(UserVO userVO) {


        long time = System.currentTimeMillis();
        User user = new User();
        BeanUtils.copyProperties(userVO, user);


        user.setPassword("123456");
        user.setStatus(1);
        user.setRegTime(time);
        user.setUpdateTime(time);
        user.setSalt(RandomUtils.getRandomString(12));
        user.setPassword(HashUtils.getMd5(user.getPassword(), user.getSalt(), 3));
        UserInfo userInfo = Security.getUserInfo();
        if (userInfo != null) {
            user.setOperator(userInfo.getUsername());
        }
        User result = userRepository.save(user);
        allocRole(result.getId(), userVO.getRoleIds());

        return result;
    }

    @Override
    public int update(UserVO userVO) {
        long time = System.currentTimeMillis();
        UserInfo userInfo = Security.getUserInfo();
        String username = "";
        if (userInfo != null) {
            username = userInfo.getUsername();
        }

        int id = userRepository.updateBaseInfo(userVO.getUsername(), userVO.getRealName(), userVO.getPhone(), userVO.getEmail(), time, username, userVO.getRemark(), userVO.getId());
        allocRole(userVO.getId(), userVO.getRoleIds());
        return id;
    }

    @Override
    public int updateCurrentUser(UserVO userVO) {
        long time = System.currentTimeMillis();
        UserInfo userInfo = Security.getUserInfo();
        String username = "";
        if (userInfo != null) {
            username = userInfo.getUsername();
        }
        int id = userRepository.updateInfo(username, userVO.getRealName(), userVO.getPhone(), userVO.getEmail(), time, username, userVO.getRemark(), userInfo.getId());
        return id;
    }

    @Override
    public void del(long id) {
        userRepository.deleteById(id);
        userRoleRepository.deleteAllByUserId(id);

    }


    @Override
    public Page<UserVO> listByPage(String requestStr, int page, int size) {
        Page<User> userPage = userRepository.findByUsernameContaining(requestStr, PageRequest.of(page, size));

        List<User> users = userPage.getContent();
        List<UserVO> userInfos = new ArrayList<>(users.size());
        for (User user : users) {
            UserVO userVO = new UserVO();
            BeanUtils.copyProperties(user, userVO);

            List<Role> roles = getAllRoleByUserId(user.getId());
            if (roles != null) {
                Set<Long> roleIds = new HashSet<>(roles.size());
                List<String> roleNames = new ArrayList<>(roleIds.size());
                for (Role role : roles) {
                    roleIds.add(role.getId());
                    roleNames.add(role.getName());
                }
                userVO.setRoleIds(roleIds);
                userVO.setPassword("xxxxxx");
                userVO.setRoles(roleNames);
            }

            userInfos.add(userVO);
        }
        return new PageImpl<>(userInfos, userPage.getPageable(), userPage.getTotalElements());
    }

    @Override
    public int changePwd(long id, String originPwd, String newPwd) {
        Optional<User> optionalUser = userRepository.findById(id);
        User user = optionalUser.orElse(null);
        if (user == null) {
            return -1;
        }
        originPwd = HashUtils.getMd5(originPwd, user.getSalt(), 3);
        if (user.getPassword().equals(originPwd)) {
            newPwd = HashUtils.getMd5(newPwd, user.getSalt(), 3);
            user.setPassword(newPwd);
            user.setUpdateTime(System.currentTimeMillis());
            userRepository.save(user);
            return 1;
        }
        return 0;
    }

    @Override
    public int findPwd(long id, String newPwd) {
        Optional<User> optionalUser = userRepository.findById(id);
        User user = optionalUser.orElse(null);
        if (user == null) {
            return -1;
        } else {
            newPwd = HashUtils.getMd5(newPwd, user.getSalt(), 3);
            user.setPassword(newPwd);
            user.setUpdateTime(System.currentTimeMillis());
            userRepository.save(user);
            return 1;
        }
    }

    @Override
    public int resetPwd(long id) {
        String randomPasswod = "123456";
        Optional<User> optionalUser = userRepository.findById(id);
        User user = optionalUser.orElse(null);
        if (user == null) {
            return -1;
        }
        randomPasswod = HashUtils.getMd5(randomPasswod, user.getSalt(), 3);
        user.setPassword(randomPasswod);
        user.setUpdateTime(System.currentTimeMillis());
        userRepository.save(user);
        return 1;
    }

    @Override
    public String login(LoginVO loginVO) {
        User user = userRepository.findByUsername(loginVO.getUsername());
        if (user == null) {
            return "-1";
        }
        String password = HashUtils.getMd5(loginVO.getPassword(), user.getSalt(), 3);
        if (user.getPassword().equals(password)) {
            user.setLastLoginTime(System.currentTimeMillis());
            User us = userRepository.save(user);
            if ("undefined".equals(loginVO.getOpenId())) {
                return "-1";
            }
            return JWTUtil.sign(loginVO.getUsername(), password);
        }
        return "0";
    }


    @Override
    public int lock(long id) {
        return userRepository.changeState(id, 0);
    }

    @Override
    public int unlock(long id) {
        return userRepository.changeState(id, 1);
    }

    @Override
    public int resign(long id) {
        return userRepository.changeState(id, -1);
    }


    @Override
    public boolean allocRole(long userId, Set<Long> roleIds) {
        userRoleRepository.deleteAllByUserId(userId);
        for (Long roleId : roleIds) {
            UserRole userRole = new UserRole();
            userRole.setRoleId(roleId);
            userRole.setUserId(userId);
            userRoleRepository.save(userRole);
        }
        return true;
    }

    @Override
    public List<Role> getAllRoleByUserId(long userId) {
        List<UserRole> userRoles = userRoleRepository.findAllByUserId(userId);
        if (userRoles != null) {
            List<Role> roles = new ArrayList<>(userRoles.size());
            for (UserRole userRole : userRoles) {
                Optional<Role> optionalRole = roleRepository.findById(userRole.getRoleId());
                optionalRole.ifPresent(roles::add);
            }
            return roles;

        }
        return null;
    }

    @Override
    public Set<Long> getAllRoleIdByUserId(long userId) {
        List<UserRole> userRoles = userRoleRepository.findAllByUserId(userId);
        if (userRoles != null) {
            Set<Long> roles = new HashSet<>(userRoles.size());
            for (UserRole userRole : userRoles) {
                Optional<Role> optionalRole = roleRepository.findById(userRole.getRoleId());
                optionalRole.ifPresent(role -> roles.add(role.getId()));
            }
            return roles;

        }
        return null;
    }

    @Override
    public UserVO getUserInfo(long userId) {
        Optional<User> optionalUser = userRepository.findById(userId);
        User user = optionalUser.orElse(null);
        if (user != null) {
            UserVO userVO = new UserVO();
            BeanUtils.copyProperties(user, userVO);
            userVO.setRoleIds(getAllRoleIdByUserId(user.getId()));

            return userVO;
        }
        return null;
    }

    @Override
    public CaptchaInfo getKaptchaImage(HttpServletRequest request,
                                       HttpServletResponse response, DefaultKaptcha captchaProducer) throws Exception {
        String capText = "";
        ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
        try {
            capText = captchaProducer.createText();
            request.getSession().setAttribute("veryCode", capText);
            //使用生产的验证码字符串返回一个BufferedImage对象并转为byte写入到byte数组中
            BufferedImage challenge = captchaProducer.createImage(capText);
            ImageIO.write(challenge, "jpg", jpegOutputStream);
        } catch (IllegalArgumentException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
        CaptchaInfo captchaInfo = new CaptchaInfo();
        captchaInfo.setImg(Base64.encodeBase64String(jpegOutputStream.toByteArray()));
        captchaInfo.setCode(HashUtils.getMd5(capText.toLowerCase(), "", 0));
        return captchaInfo;
    }

    @Override
    public boolean checkVeryCode(String code, String veryCode) {
        log.info("[checkVeryCode] code={} veryCode={}", code, veryCode);
        if (code.toLowerCase().equals(veryCode.toLowerCase())) {
            return true;
        }
        return false;
    }


}
