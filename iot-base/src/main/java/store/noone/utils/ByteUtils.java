package store.noone.utils;

/**
 * @author 刘晓东
 */
public class ByteUtils {

    public static byte getByteByBits(byte[] bytes, int from, int to) {
        byte value = 0;
        for (int i = from; i >= to; i--) {
            value += ((bytes[i] << (from - i)) & 0xff);
        }
        return value;
    }

    public static byte[] getBits(byte b) {
        byte[] array = new byte[8];
        for (int i = 7; i >= 0; i--) {
            array[i] = (byte) (b & 1);
            b = (byte) (b >> 1);
        }
        return array;
    }

    public static byte[] getBits(int b) {
        byte[] array = new byte[16];
        for (int i = 15; i >= 0; i--) {
            array[i] = (byte) (b & 1);
            b = (byte) (b >> 1);
        }
        return array;
    }

    public static byte[] getByteBits(byte b) {
        byte[] array = new byte[8];
        for (int i = 7; i >= 0; i--) {
            array[i] = (byte) (b & 1);
            b = (byte) (b >> 1);
        }
        return array;
    }

    public static int getIntByBits(byte[] low, byte[] high) {
        int value = 0;
        for (int i = 7; i >= 0; i--) {
            value += (low[i] << (7 - i)) & 0xffff;
        }
        for (int i = 7; i >= 0; i--) {
            value += (high[i] << (15 - i)) & 0xffff;
        }
        return value;
    }

    /**
     * 低位在前，高位在后
     * @param a
     * @return byte
     */
    public static byte[] short2LHByte(short a){
        byte[] b = new byte[2];

        b[1] = (byte) (a >> 8);
        b[0] = (byte) (a);

        return b;
    }

    /**
     * 低位在前，高位在后
     * @param a
     * @return byte
     */
    public static byte[] int2LHByte(int a){
        byte[] b = new byte[4];
        b[0] = (byte) (a & 0x0FF);
        b[1] = (byte) (a >> 8 & 0x0FF);
        b[2] = (byte) (a >> 16 & 0x0FF);
        b[3] = (byte) (a >> 24 & 0x0FF);
        return b;
    }

    /**
     * 16进制字符串转byte数组 0f=>15
     * @param hexString
     * @return byte[]
     */
    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    public static String bytes2HexString(byte[] b) {
        StringBuffer result = new StringBuffer();
        String hex;
        for (int i = 0; i < b.length; i++) {
            hex = Integer.toHexString(b[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            result.append(hex.toUpperCase());
        }
        return result.toString();
    }

    public static int bytes2int(byte[] bytes) {
        int result = 0;
        if (bytes.length == 4) {
            int a = (bytes[3] & 0xff) << 24;
            int b = (bytes[2] & 0xff) << 16;
            int c = (bytes[1] & 0xff) << 8;
            int d = (bytes[0] & 0xff);
            result = a | b | c | d;
        }
        return result;
    }

    /**
     * Convert char to byte
     * @param c char
     * @return byte
     */
    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

    public static void main(String[] args) {
        byte[] low = new byte[4];
        low[0] = 1;
        low[1] = 1;
        low[2] = 1;
        low[3] = 1;

        byte[] high = new byte[8];
        high[6] = 1;
        high[7] = 1;

        short ret = (short) 65525;
        System.out.println(ret);
    }
}
