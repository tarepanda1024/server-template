package store.noone.ice.utils;

import me.chanjar.weixin.mp.util.json.WxMpGsonBuilder;

/**
 * Created with IDEA 2018
 *
 * @author Kang
 */
public class JsonUtils {
    public static String toJson(Object obj) {
        return WxMpGsonBuilder.create().toJson(obj);
    }
}
