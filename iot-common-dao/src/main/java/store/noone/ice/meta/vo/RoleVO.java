package store.noone.ice.meta.vo;

import lombok.Data;

import java.util.List;

/**
 * @author 刘晓东(liuxd2017 @ 163.com)
 */
@Data
public class RoleVO {
    private Long id;
    private String name;
    private int status;
    private String operator;
    private String remark;
    private long createTime;
    private long updateTime;
    private List<Integer> permIds;
}
