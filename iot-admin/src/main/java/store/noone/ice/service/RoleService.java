package store.noone.ice.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.data.domain.Page;
import store.noone.ice.meta.po.Permission;
import store.noone.ice.meta.po.Role;
import store.noone.ice.meta.vo.RoleVO;

import java.util.List;
import java.util.Set;

public interface RoleService {

    int insert(RoleVO roleVO);

    int update(RoleVO roleVO);

    Page<RoleVO> listByPage(String requestStr, int page, int size);

    int lock(long id);

    int unlock(long id);

    void del(long id);

    boolean allocPerm(long roleId, List<Integer> permIds);

    List<Permission> getAllPermsByRoleId(long roleId);

    Set<Integer> getAllPermIdByRoleIds(Set<Long> roleIds);

    JSONObject  getAllRoles(String prefix);

    Role getById(long id);
}
