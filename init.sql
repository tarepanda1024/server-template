
create table permission
(
  id         bigint auto_increment not null,
  parentId   bigint                not null COMMENT '父权限id',
  permId     int                   null     COMMENT '权限id',
  needSuperRole       tinyint      not null default '0' COMMENT '是否超级管理员权限',
  type       int                   not null COMMENT '权限类型',
  name       varchar(64)           not null COMMENT '权限名称',
  url        varchar(128)          null     COMMENT '前端路由',
  permission varchar(64)           not null COMMENT '权限表达式',
  createTime bigint                not null COMMENT '创建时间',
  updateTime bigint                not null COMMENT '更新时间',
  operator   varchar(64)           null     COMMENT '操作人',
  remark     varchar(128)          null     COMMENT '备注',

  PRIMARY KEY (`id`),
  KEY `permission` (`permission`),
  UNIQUE KEY `permId` (`permId`)
)ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT ='权限表';

create table role
(
  id         bigint auto_increment not null,
  name       varchar(64)           not null            COMMENT '权限名称',
  status     int                   not null,
  createTime bigint                not null            COMMENT '创建时间',
  updateTime bigint                not null            COMMENT '更新时间',
  operator   varchar(64)           null                COMMENT '操作人',
  remark     varchar(128)          null                COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
)ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT ='角色表';


create table role_permission
(
  id     bigint auto_increment not null,
  permId int                   not null,
  roleId bigint                not null,
  PRIMARY KEY (`id`)
)ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT ='角色权限表';

create table user_role
(
  id     bigint auto_increment not null,
  roleId bigint                not null,
  userId bigint                not null,
  PRIMARY KEY (`id`)
)ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT ='用户角色表';


create table user
(
  id            bigint auto_increment not null,
  username      varchar(64)           not null            COMMENT '用户名',
  realName      varchar(12)           not null            COMMENT '真实姓名',
  email         varchar(64)           null                COMMENT '邮箱',
  salt          varchar(64)           not null            COMMENT '密码加盐',
  password      varchar(32)           not null            COMMENT 'md5密码',
  phone         varchar(11)           not null            COMMENT '手机号',
  status        int                   not null            COMMENT '状态',
  regTime       bigint                not null            COMMENT '注册时间',
  lastLoginTime bigint                not null            COMMENT '最后登录时间',
  updateTime    bigint                not null            COMMENT '更新时间',
  remark      varchar(64)           not null default ''   COMMENT '备注',
  operator      varchar(64)           null                COMMENT '操作人',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
)ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT ='用户表';


create table tb_iot_opt_log
(
  id         bigint auto_increment not null,
  createTime bigint                not null,
  operator   varchar(36)           null,
  optType    int                   not null,
  remark     varchar(256)          null,
  PRIMARY KEY (`id`)
)ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT ='操作信息表';




create table tb_site
(
  id       bigint auto_increment not null,
  siteName varchar(120)          not null,
  corpName varchar(120)          not null,
  copyInfo varchar(120)          not null,
  version  varchar(18)           not null,
  PRIMARY KEY (`id`)
)ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT ='站点信息表';


