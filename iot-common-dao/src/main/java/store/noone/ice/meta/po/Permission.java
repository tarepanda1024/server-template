package store.noone.ice.meta.po;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "permission")
public class Permission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int permId;
    private long parentId;
    private int needSuperRole;//是否需要超级管理员才可以使用
    private int type;//资源类型
    private String name;
    private String permission;
    private String url;
    private String remark;
    private long createTime;
    private long updateTime;
    private String operator;
}
