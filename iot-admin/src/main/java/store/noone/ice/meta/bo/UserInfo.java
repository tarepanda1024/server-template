package store.noone.ice.meta.bo;

import lombok.Data;
import store.noone.ice.meta.po.Role;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Data
public class UserInfo implements Serializable {
    private long id;
    private long corpId;
    private String prefix;
    private String username;
    private String openId;
    private String password;
    private String salt;
    private String email;
    private String phone;
    private List<Role> roles;
    private Set<Long> roleIds;
    private Set<Integer> permIds;
    private Set<String> roleNames;
    private Set<String> permissions;
}
