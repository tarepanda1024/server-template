package store.noone.ice.service.utils;


/**
 * Created with IDEA 2018
 *
 * @author Kang
 */
public interface MailService {
    void sendSimpleMail(String to, String subject, String content);

    void sendTemplateMail(String to, String subject, String content);
}
