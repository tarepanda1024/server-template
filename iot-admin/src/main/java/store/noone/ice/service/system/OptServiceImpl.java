package store.noone.ice.service.system;

import lombok.extern.slf4j.Slf4j;
import store.noone.ice.dao.OptLogRepository;
import store.noone.ice.meta.bo.UserInfo;
import store.noone.ice.meta.enums.OptTypeEnum;
import store.noone.ice.meta.po.OptLog;
import store.noone.ice.security.Security;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 * @author 刘晓东
 */
@Slf4j
@Service
public class OptServiceImpl implements OptService {
    @Resource
    private OptLogRepository optLogRepository;


    @Override
    public void remark(OptTypeEnum optType, String logMsg) {
        UserInfo userInfo = Security.getUserInfo();
        if (userInfo != null) {
            remark(optType, userInfo.getUsername(), logMsg);
        } else {
            log.error("[remark] userinfo is null");
        }
    }

    @Override
    public void remark(OptTypeEnum optType, String username, String logMsg) {
        OptLog optLog = new OptLog();
        optLog.setCreateTime(System.currentTimeMillis());
        optLog.setOptType(optType.getType());
        optLog.setRemark(logMsg);
        optLog.setOperator(username);
        optLogRepository.save(optLog);
    }

    @Override
    public Page<OptLog> listByPage(String requestStr, int page, int size) {
        return optLogRepository.findByRemarkContainingOrderByCreateTimeDesc(requestStr, PageRequest.of(page, size));
    }
}
