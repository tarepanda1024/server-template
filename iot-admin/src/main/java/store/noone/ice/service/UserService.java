package store.noone.ice.service;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import org.springframework.data.domain.Page;
import store.noone.ice.meta.bo.CaptchaInfo;
import store.noone.ice.meta.bo.UserInfo;
import store.noone.ice.meta.po.Role;
import store.noone.ice.meta.po.User;
import store.noone.ice.meta.vo.LoginVO;
import store.noone.ice.meta.vo.UserVO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Set;

public interface UserService {
    UserInfo findByUsername(String username);

    User insert(UserVO userVO);

    int update(UserVO userVO);
    int updateCurrentUser(UserVO userVO);

    void del(long id);

    Page<UserVO> listByPage( String requestStr, int page, int size);

    int changePwd(long id, String originPwd, String newPwd);

    int findPwd(long id, String newPwd);

    int resetPwd(long id);

    String login(LoginVO loginVO);


    //锁定
    int lock(long id);

    //解锁
    int unlock(long id);

    //辞职
    int resign(long id);

    boolean allocRole(long userId, Set<Long> roleIds);

    List<Role> getAllRoleByUserId(long userId);

    Set<Long> getAllRoleIdByUserId(long userId);

    UserVO getUserInfo(long userId);

    //验证码

    CaptchaInfo getKaptchaImage(HttpServletRequest request,
                                HttpServletResponse response, DefaultKaptcha captchaProducer) throws Exception;

    boolean checkVeryCode(String code, String veryCode);



}
