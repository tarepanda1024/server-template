package store.noone.ice.utils;


import java.math.BigDecimal;

/**
 * Created with IDEA 2019
 *
 * @author Kang
 */
public class ArithUtils {
    /**
     * 除法精度3位 结果精度2位 四舍五入 五向上入
     */
    private static final int DEF_DIV_SCALE = 3;
    private static final int RESULT_DIV_SCALE = 2;
    private static final int ROUND_MODE = BigDecimal.ROUND_HALF_UP;

    ArithUtils() {
    }

    private static BigDecimal result(BigDecimal b) {
        return b.setScale(RESULT_DIV_SCALE, ROUND_MODE);
    }

    public static double strToDouble(String v) {
        BigDecimal b1 = new BigDecimal(v);
        return result(b1).doubleValue();
    }

    public static double add(double v1, double v2) {
        BigDecimal b1 = BigDecimal.valueOf(v1);
        BigDecimal b2 = BigDecimal.valueOf(v2);
        return result(b1.add(b2)).doubleValue();
    }

    public static double add(float v1, float v2) {
        BigDecimal b1 = new BigDecimal(Float.toString(v1));
        BigDecimal b2 = new BigDecimal(Float.toString(v2));
        return result(b1.add(b2)).doubleValue();
    }

    public static double add(float v1, double v2) {
        BigDecimal b1 = new BigDecimal(Float.toString(v1));
        BigDecimal b2 = BigDecimal.valueOf(v2);
        return result(b1.add(b2)).doubleValue();
    }

    public static double add(double v1, float v2) {
        BigDecimal b1 = BigDecimal.valueOf(v1);
        BigDecimal b2 = new BigDecimal(Float.toString(v2));
        return result(b1.add(b2)).doubleValue();
    }

    public static double sub(double v1, double v2) {
        BigDecimal b1 = BigDecimal.valueOf(v1);
        BigDecimal b2 = BigDecimal.valueOf(v2);
        return result(b1.subtract(b2)).doubleValue();
    }

    public static double sub(float v1, float v2) {
        BigDecimal b1 = new BigDecimal(Float.toString(v1));
        BigDecimal b2 = new BigDecimal(Float.toString(v2));
        return result(b1.subtract(b2)).doubleValue();
    }

    public static double sub(float v1, double v2) {
        BigDecimal b1 = new BigDecimal(Float.toString(v1));
        BigDecimal b2 = BigDecimal.valueOf(v2);
        return result(b1.subtract(b2)).doubleValue();
    }

    public static double sub(double v1, float v2) {
        BigDecimal b1 = BigDecimal.valueOf(v1);
        BigDecimal b2 = new BigDecimal(Float.toString(v2));
        return result(b1.subtract(b2)).doubleValue();
    }

    public static double mul(double v1, double v2) {
        BigDecimal b1 = BigDecimal.valueOf(v1);
        BigDecimal b2 = BigDecimal.valueOf(v2);

        return result(result(b1.multiply(b2))).doubleValue();
    }

    public static double mul(float v1, float v2) {
        BigDecimal b1 = new BigDecimal(Float.toString(v1));
        BigDecimal b2 = new BigDecimal(Float.toString(v2));

        return result(result(b1.multiply(b2))).doubleValue();
    }

    public static double mul(float v1, double v2) {
        BigDecimal b1 = new BigDecimal(Float.toString(v1));
        BigDecimal b2 = BigDecimal.valueOf(v2);

        return result(result(b1.multiply(b2))).doubleValue();
    }

    public static double mul(double v1, float v2) {
        BigDecimal b1 = BigDecimal.valueOf(v1);
        BigDecimal b2 = new BigDecimal(Float.toString(v2));

        return result(result(b1.multiply(b2))).doubleValue();
    }

    public static double div(double v1, double v2) {
        BigDecimal b1 = BigDecimal.valueOf(v1);
        BigDecimal b2 = BigDecimal.valueOf(v2);
        return result(b1.divide(b2, DEF_DIV_SCALE, ROUND_MODE))
                .doubleValue();
    }

    public static double div(float v1, float v2) {
        BigDecimal b1 = new BigDecimal(Float.toString(v1));
        BigDecimal b2 = new BigDecimal(Float.toString(v2));
        return result(b1.divide(b2, DEF_DIV_SCALE, ROUND_MODE))
                .doubleValue();
    }

    public static double div(float v1, double v2) {
        BigDecimal b1 = new BigDecimal(Float.toString(v1));
        BigDecimal b2 = BigDecimal.valueOf(v2);
        return result(b1.divide(b2, DEF_DIV_SCALE, ROUND_MODE))
                .doubleValue();
    }

    public static double div(double v1, float v2) {
        BigDecimal b1 = BigDecimal.valueOf(v1);
        BigDecimal b2 = new BigDecimal(Float.toString(v2));
        return result(b1.divide(b2, DEF_DIV_SCALE, ROUND_MODE))
                .doubleValue();
    }

    public static double div(double v1, double v2, int scale, int roundingMode) {
        BigDecimal b1 = BigDecimal.valueOf(v1);
        BigDecimal b2 = BigDecimal.valueOf(v2);
        return result(b1.divide(b2, scale, roundingMode)).doubleValue();
    }

    public static int compareTo(double v1, double v2) {
        BigDecimal b1 = BigDecimal.valueOf(v1);
        BigDecimal b2 = BigDecimal.valueOf(v2);
        return b1.compareTo(b2);
    }

    public static int compareTo(float v1, float v2) {
        BigDecimal b1 = new BigDecimal(Float.toString(v1));
        BigDecimal b2 = new BigDecimal(Float.toString(v2));
        return b1.compareTo(b2);
    }

    public static int compareTo(double v1, float v2) {
        BigDecimal b1 = BigDecimal.valueOf(v1);
        BigDecimal b2 = new BigDecimal(Float.toString(v2));
        return b1.compareTo(b2);
    }

    public static int compareTo(float v1, double v2) {
        BigDecimal b1 = new BigDecimal(Float.toString(v1));
        BigDecimal b2 = BigDecimal.valueOf(v2);
        return b1.compareTo(b2);
    }

    public static double format(double v, int scale) {
        BigDecimal b1 = BigDecimal.valueOf(v)
                .setScale(scale, ROUND_MODE);
        return b1.doubleValue();
    }

    public static double format(float v, int scale) {
        BigDecimal b1 = new BigDecimal(Float.toString(v))
                .setScale(scale, ROUND_MODE);
        return b1.doubleValue();
    }
}
