package store.noone.ice.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import store.noone.ice.meta.po.RolePermission;

import java.util.List;

@Repository
public interface RolePermissionRepository extends CrudRepository<RolePermission, Long> {

    List<RolePermission> findAllByRoleId(long roleId);

    @Transactional
    int deleteAllByRoleId(long roleId);
}
