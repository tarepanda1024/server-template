package store.noone.ice.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import store.noone.ice.meta.po.Permission;

import java.util.List;
import java.util.Optional;

@Repository
public interface PermissionRepository extends PagingAndSortingRepository<Permission, Long> {
    @Query("select perm from Permission perm where perm.parentId = ?1")
    List<Permission> findAllByParentId(long parentId);

    @Override
    Optional<Permission> findById(Long id);

    Optional<Permission> findByPermId(Integer permId);
}
