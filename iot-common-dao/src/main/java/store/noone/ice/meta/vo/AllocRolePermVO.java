package store.noone.ice.meta.vo;

import lombok.Data;

import java.util.List;

@Data
public class AllocRolePermVO {
    private Long roleId;
    private List<Integer> permIds;
}
