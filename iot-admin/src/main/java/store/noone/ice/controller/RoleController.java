package store.noone.ice.controller;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import store.noone.ice.meta.AjaxResult;
import store.noone.ice.meta.vo.AllocRolePermVO;
import store.noone.ice.meta.vo.QueryVO;
import store.noone.ice.meta.vo.RoleVO;
import store.noone.ice.security.Security;
import store.noone.ice.service.RoleService;

import java.util.Map;

@RestController
@RequestMapping("/xhr/admin/role")
@Slf4j
public class RoleController {
    private final RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @PostMapping("/insert")
    @RequiresPermissions("role:insert")
    public AjaxResult insert(@RequestBody RoleVO roleVO) {
        log.info("[insert] begin insert role ={}", roleVO);
        if (StringUtils.isEmpty(roleVO.getName())) {
            return new AjaxResult<>(HttpStatus.BAD_REQUEST.value(), "参数非法", null);
        }
        int retObj = roleService.insert(roleVO);
        if (retObj < 1) {
            log.info("[insert] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[insert] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", retObj);
        }
    }

    @PostMapping("/update")
    @RequiresPermissions("role:edit")
    public AjaxResult update(@RequestBody RoleVO roleVO) {
        log.info("[update] begin update roleVO ={}", roleVO);
        if (StringUtils.isEmpty(roleVO.getName())) {
            return new AjaxResult<>(HttpStatus.BAD_REQUEST.value(), "参数非法", null);
        }
        int retObj = roleService.update(roleVO);
        if (retObj < 1) {
            log.info("[update] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[update] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", retObj);
        }
    }

    @PostMapping("/allocPerms")
    @RequiresPermissions("role:edit")
    public AjaxResult allocPerms(@RequestBody AllocRolePermVO allocRolePermVO) {
        log.info("[update] begin alloc perm ={}", allocRolePermVO);
        if (StringUtils.isEmpty(allocRolePermVO.getRoleId())) {
            return new AjaxResult<>(HttpStatus.BAD_REQUEST.value(), "参数非法", null);
        }
        if (roleService.allocPerm(allocRolePermVO.getRoleId(), allocRolePermVO.getPermIds())) {
            log.info("[update] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", null);

        } else {
            log.info("[update] failed.");
            return AjaxResult.initFailMsg("内部错误");
        }
    }

    @PostMapping("/list")
    @RequiresPermissions("role:view")
    public AjaxResult list(@RequestBody QueryVO queryVO) {
        if (queryVO.getPage() < 1 || queryVO.getSize() < 0) {
            log.error("[]");
        }

        Page<RoleVO> rolePage = roleService.listByPage( queryVO.getQueryStr(), queryVO.getPage() - 1, queryVO.getSize());
        if (rolePage == null) {
            log.info("[list] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[list] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", rolePage);
        }
    }

    @PostMapping("/getAll")
    @RequiresPermissions("role:view")
    public AjaxResult getAll(@RequestBody Map<String, String> prefix) {
        JSONObject roles = roleService.getAllRoles(prefix.get("prefix"));
        if (roles == null) {
            log.info("[list] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[list] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", roles);
        }
    }

    @GetMapping("/lock")
    @RequiresPermissions("role:state")
    public AjaxResult lock(@RequestParam long id) {
        int count = roleService.lock(id);
        if (count < 1) {
            log.info("[lock] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[lock] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", count);
        }
    }

    @GetMapping("/unlock")
    @RequiresPermissions("role:state")
    public AjaxResult unlock(@RequestParam long id) {
        int count = roleService.unlock(id);
        if (count < 1) {
            log.info("[lock] failed.");
            return AjaxResult.initFailMsg("内部错误");
        } else {
            log.info("[lock] success.");
            return new AjaxResult<>(HttpStatus.OK.value(), "success", count);
        }
    }

    @GetMapping("/del")
    @RequiresPermissions("role:del")
    public AjaxResult del(@RequestParam long id) {
        log.info("[del] del perm id={}", id);
        roleService.del(id);
        return AjaxResult.initOkMsg("success");
    }

}
